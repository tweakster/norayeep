use std::env;

use crate::mastodon::{MastodonClient, Status};
use crate::post::CategoryAlias;
use crate::post::Post;
use crate::post::Tag;
use crate::post::TagPriority;

mod mastodon;
mod post;
mod rss;

const MAX_POST_SIZE: usize = 400;

struct RSSConfig {
    pub url: String,
    pub tags: Vec<Tag>,
    pub category_alias: Vec<CategoryAlias>,
    pub inc_description: bool,
}

struct Settings {
    pub dry_run: bool,
    pub feed: Option<RSSConfig>,
}

fn get_var(name: &str) -> Result<String, String> {
    match env::var(name) {
        Ok(v) => return Ok(v),
        Err(_) => Err(format!("{} variable not found", name)),
    }
}

fn fetch_mastodon_account_id(client: &MastodonClient) -> Result<String, String> {
    let result = client.get_account_credentials();
    match result {
        Ok(ca) => return Ok(ca.id),
        Err(mess) => return Err(format!("Cannot get Mastodon account details: {} ", mess)),
    }
}

fn fetch_status_feed(client: &MastodonClient) -> Result<Vec<Status>, String> {
    let mastodon_id = fetch_mastodon_account_id(&client)?;
    let feed = client.get_status_feed(mastodon_id);

    match feed {
        Err(e) => return Err(format!("Error fetching status feed: {}", e)),
        Ok(f) => return Ok(f),
    }
}

fn post_new_content(
    client: &MastodonClient,
    posts: &Vec<Post>,
    status_feed: &Vec<Status>,
    dry_run: bool,
) -> i32 {
    let mut posted = 0;

    for p in posts {
        let mut found = false;
        for s in status_feed {
            if s.content_external_href() == Some(p.href.clone()) {
                found = true;
                break;
            }
        }
        if !found {
            println!("Posting {}", p.href);
            if dry_run {
                println!("{}", p.to_string(MAX_POST_SIZE));
            } else {
                client.post_status(p.to_string(MAX_POST_SIZE));
            }
            posted += 1;
        }
    }

    posted
}

fn parse_args() -> Settings {
    let mut settings = Settings {
        dry_run: false,
        feed: None,
    };

    let mut args = env::args().skip(1);

    while let Some(a) = args.next() {
        match a.as_str() {
            "--feed-novara" => {
                settings.feed = Some(RSSConfig {
                    url: "https://novaramedia.com/rss".to_string(),
                    category_alias: vec![
                        CategoryAlias::new("ACFM".to_string(), vec!["Acfm".to_string()]),
                        CategoryAlias::new("NovaraFM".to_string(), vec!["NovaraFm".to_string()]),
                        CategoryAlias::new(
                            "NovaraDownstream".to_string(),
                            vec!["Downstream".to_string()],
                        ),
                    ],
                    tags: vec![
                        Tag::new("Novara".to_string(), TagPriority::Must),
                        Tag::new("ACFM".to_string(), TagPriority::Optional),
                        Tag::new("NovaraDownstream".to_string(), TagPriority::Optional),
                        Tag::new("IfISpeak".to_string(), TagPriority::Optional),
                        Tag::new("NovaraFM".to_string(), TagPriority::Optional),
                        Tag::new("NovaraLive".to_string(), TagPriority::Optional),
                    ],
                    inc_description: false,
                })
            }
            "--feed-zeteo" => {
                settings.feed = Some(RSSConfig {
                    url: "https://zeteo.com/feed".to_string(),
                    category_alias: vec![],
                    tags: vec![Tag::new("Zeteo".to_string(), TagPriority::Must)],
                    inc_description: true,
                })
            }
            "--dry-run" => settings.dry_run = true,
            _ => println!("{}", a),
        };
    }

    return settings;
}

fn main() -> Result<(), String> {
    let settings = parse_args();

    let domain = get_var("MAST_DOMAIN")?;
    let token = get_var("MAST_TOKEN")?;

    let posts = match settings.feed {
        None => return Err(format!("No feed selected")),
        Some(f) => rss::extract_latest(f.url, f.tags, f.category_alias, 6, f.inc_description),
    };

    let mastodon = MastodonClient::create(domain, token);

    let status_feed = fetch_status_feed(&mastodon)?;

    println!("{:#?}", posts);

    let posted = post_new_content(&mastodon, &posts, &status_feed, settings.dry_run);

    println!(
        "Completed. Posts Found: {}, Status Posts Made: {}",
        posts.len(),
        posted
    );
    Ok(())
}
