use std::cmp;

use rss::Channel;

use crate::post::CategoryAlias;
use crate::post::Post;
use crate::post::Tag;
use crate::post::TagPriority;

pub fn extract_latest(
    feed: String,
    tags: Vec<Tag>,
    category_alias: Vec<CategoryAlias>,
    max_posts: usize,
    inc_description: bool,
) -> Vec<Post> {
    let mut posts: Vec<Post> = vec![];

    let content = reqwest::blocking::get(&feed).unwrap().bytes().unwrap();
    let channel = Channel::read_from(&content[..]).unwrap();

    for item in channel.items {
        let text_parts = if inc_description {
            vec![item.title.unwrap_or("".to_string())]
        } else {
            vec![
                item.title.unwrap_or("".to_string()),
                item.description.unwrap_or("".to_string()),
            ]
        };

        let categories = item.categories;
        let mut final_tags: Vec<Tag> = tags
            .iter()
            .cloned()
            .filter(|t| t.is_priority(TagPriority::Must))
            .collect();

        for c in categories {
            let mut trimmed: String = Tag::tag_case(c.name);

            for ca in &category_alias {
                for a in &(ca.categories) {
                    if trimmed == *a {
                        trimmed = ca.tag.clone();
                        break;
                    }
                }
            }

            for t in &tags {
                if &trimmed == t.name() {
                    let index = final_tags.iter().position(|ft| *(ft.name()) == *trimmed);
                    if index.is_none() {
                        final_tags.push(t.clone());
                    }
                    break;
                }
            }
        }

        posts.push(Post {
            href: item.link.unwrap_or("".to_string()),
            text: text_parts.join("\n\n"),
            tags: final_tags,
        });
    }

    let max_return = cmp::min(posts.len(), max_posts);

    return posts[..max_return].to_vec();
}
