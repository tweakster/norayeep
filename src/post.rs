pub struct CategoryAlias {
    pub tag: String,
    pub categories: Vec<String>,
}

impl CategoryAlias {
    pub fn new(tag: String, aliases: Vec<String>) -> CategoryAlias {
        return CategoryAlias {
            tag,
            categories: aliases,
        };
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum TagPriority {
    Must,
    Optional,
}

#[derive(Clone, Debug)]
pub struct Tag {
    name: String,
    priority: TagPriority,
}

impl Tag {
    pub fn new(name: String, priority: TagPriority) -> Tag {
        return Tag { name, priority };
    }

    pub fn is_priority(&self, priority: TagPriority) -> bool {
        return self.priority == priority;
    }

    pub fn to_string(&self) -> String {
        return format!("#{}", self.name);
    }

    pub fn name(&self) -> &String {
        return &(self.name);
    }

    pub fn tag_case(text: String) -> String {
        let mut out: Vec<char> = vec![];
        let mut prev_space = true;
        for c in text.chars() {
            if c == ' ' {
                prev_space = true;
            } else {
                if prev_space {
                    out.push(c.to_ascii_uppercase());
                    prev_space = false;
                } else {
                    out.push(c.to_ascii_lowercase());
                }
            }
        }
        return out.into_iter().collect::<String>();
    }
}

#[derive(Clone, Debug)]
pub struct Post {
    pub href: String,
    pub text: String,
    pub tags: Vec<Tag>,
}

impl Post {
    pub fn to_string(&self, max_length: usize) -> String {
        let mut out: Vec<String> = vec![];

        'builder: {
            let mut len = self.href.len() + 2; // Allowing for 2 next line chars
            if len > max_length {
                break 'builder;
            }
            out.push(format!("{}\n\n", self.href));

            let text_len = self.text.len();
            let mut out_tags: Vec<String> = vec![];
            'tags: for priority in vec![TagPriority::Must, TagPriority::Optional] {
                let opt_text_len = if priority == TagPriority::Must {
                    0
                } else {
                    text_len
                };
                for tag in &self.tags {
                    if tag.is_priority(priority) {
                        let tag_string = tag.to_string();
                        if len + tag_string.len() + 1 + opt_text_len > max_length {
                            break 'tags;
                        }
                        len = len + tag_string.len() + 1;
                        out_tags.push(tag_string);
                    }
                }
            }

            if out_tags.len() > 0 {
                out.push(format!("{}\n", out_tags.join(" ")));
            }

            if (len + 2) >= max_length {
                break 'builder;
            }

            let remain_len = max_length - len - 2;
            let remain_text = if remain_len >= text_len {
                &self.text
            } else {
                &self.text[..remain_len]
            };
            out.insert(0, format!("{}\n\n", remain_text));
        }
        return out.join("");
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_tag_case_format() -> () {
        struct TestNameFormat {
            raw: String,
            expected: String,
        }

        let tests: Vec<TestNameFormat> = vec![
            TestNameFormat {
                raw: "one".to_string(),
                expected: "One".to_string(),
            },
            TestNameFormat {
                raw: "two words".to_string(),
                expected: "TwoWords".to_string(),
            },
            TestNameFormat {
                raw: "single i middle".to_string(),
                expected: "SingleIMiddle".to_string(),
            },
            TestNameFormat {
                raw: "mIxeD CaSE".to_string(),
                expected: "MixedCase".to_string(),
            },
            TestNameFormat {
                raw: "ALL UPPER".to_string(),
                expected: "AllUpper".to_string(),
            },
            TestNameFormat {
                raw: "rand0m !£%& char$".to_string(),
                expected: "Rand0m!£%&Char$".to_string(),
            },
        ];

        for t in tests {
            let parsed = Tag::tag_case(t.raw);
            assert_eq!(parsed, t.expected);
        }
    }

    #[test]
    fn test_post_to_string_no_tags() -> () {
        let post = Post {
            href: "https://example.com/".to_string(),
            text: "This is my site".to_string(),
            tags: vec![],
        };

        let expected = "This is my site\n\nhttps://example.com/\n\n";
        assert_eq!(post.to_string(1000), expected);
    }

    #[test]
    fn test_post_to_string_with_all_tags() -> () {
        let post = Post {
            href: "https://example.com/".to_string(),
            text: "This is my site".to_string(),
            tags: vec![
                Tag::new("Three".to_string(), TagPriority::Optional),
                Tag::new("Two".to_string(), TagPriority::Optional),
                Tag::new("One".to_string(), TagPriority::Must),
            ],
        };

        // Tags should be ordered by priority in output. Optional is ignored
        let expected = "This is my site\n\nhttps://example.com/\n\n#One #Three #Two\n";
        assert_eq!(post.to_string(1000), expected);
    }

    #[test]
    fn test_post_to_string_strip_some_tags() -> () {
        let post = Post {
            href: "https://example.com/".to_string(),
            text: "This is my site".to_string(),
            tags: vec![
                Tag::new("Two".to_string(), TagPriority::Optional),
                Tag::new("Three".to_string(), TagPriority::Optional),
                Tag::new("One".to_string(), TagPriority::Must),
            ],
        };

        // Only room for one of the tags
        let expected = "This is my site\n\nhttps://example.com/\n\n#One\n";
        assert_eq!(post.to_string(45), expected);
    }

    #[test]
    fn test_post_to_string_strip_content() -> () {
        let post = Post {
            href: "https://example.com/".to_string(),
            text: "This is my site".to_string(),
            tags: vec![
                Tag::new("One".to_string(), TagPriority::Must),
                Tag::new("Two".to_string(), TagPriority::Optional),
                Tag::new("Three".to_string(), TagPriority::Optional),
            ],
        };

        // Must tags come before text
        let expected = "This i\n\nhttps://example.com/\n\n#One\n";
        assert_eq!(post.to_string(35), expected);
    }

    #[test]
    fn test_post_to_string_drop_should_tags() -> () {
        let post = Post {
            href: "https://example.com/".to_string(),
            text: "This is my site".to_string(),
            tags: vec![
                Tag::new("One".to_string(), TagPriority::Must),
                Tag::new("Two".to_string(), TagPriority::Optional),
                Tag::new("Three".to_string(), TagPriority::Optional),
            ],
        };

        // Must tags come before text
        let expected = "This is my site\n\nhttps://example.com/\n\n#One\n";
        assert_eq!(post.to_string(46), expected);
    }

    #[test]
    fn test_post_to_string_too_many_must_tags() -> () {
        let post = Post {
            href: "https://example.com/".to_string(),
            text: "This is my site".to_string(),
            tags: vec![
                Tag::new("One".to_string(), TagPriority::Must),
                Tag::new("Two".to_string(), TagPriority::Must),
                Tag::new("Three".to_string(), TagPriority::Must),
            ],
        };

        // Add must tags in order
        let expected = "https://example.com/\n\n#One #Two\n";
        assert_eq!(post.to_string(34), expected);
    }
}
