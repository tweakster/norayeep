locals {
  jobs = [
    {
      name               = "novara"
      command_args       = "--feed-novara"
      mastodon_domain    = var.novara_mastodon_domain
      mastodon_api_token = var.novara_mastodon_api_token
      schedules = [
        {
          name        = "day"
          description = "Day schedule, Mastodon. Primarily: Audio, Posts"
          when        = "10 12-18 * * *"
        },
        {
          name        = "eve"
          description = "Evening schedule, Mastodon. Primarily: Norva Live, Download"
          when        = "*/15 19,20 * * 0-5"
        },
      ]
    },
    {
      name               = "zeteo"
      command_args       = "--feed-zeteo"
      mastodon_domain    = var.zeteo_mastodon_domain
      mastodon_api_token = var.zeteo_mastodon_api_token
      schedules = [
        {
          name        = "day"
          description = "Zeto schedule, Mastodon"
          when        = "40 12-20 * * *"
        }
      ]
    }
  ]
}

locals {
  job_records = {
    for j in local.jobs : j.name => j
  }
  schedules = flatten([
    for j in local.jobs : [
      for s in j.schedules : merge(
        s,
        {
          job_name = j.name
        }
      )
    ]
  ])
  schedule_records = {
    for s in local.schedules : "${s.job_name}_${s.name}" => s
  }
}

resource "google_service_account" "this" {
  account_id   = "mastodon-intermediator-${var.env}"
  display_name = "Mastodon Intermediator ${title(var.env)} access"
}

resource "google_project_iam_member" "this" {
  project = var.cloud_project

  member = "serviceAccount:${google_service_account.this.email}"
  role   = "roles/run.invoker"
}

resource "google_cloud_run_v2_job" "this" {
  for_each = local.job_records

  name = "mastodon-intermediator-${each.key}-${var.env}"

  location = var.cloud_region

  template {
    template {
      containers {
        image   = "${var.image_repository}:${var.app_version}"
        command = ["/usr/bin/norayeep", each.value.command_args]

        env {
          name  = "MAST_DOMAIN"
          value = each.value.mastodon_domain
        }

        env {
          name  = "MAST_TOKEN"
          value = each.value.mastodon_api_token
        }

        resources {
          limits = {
            cpu    = "1000m"
            memory = "512Mi"
          }
        }
      }

      service_account = google_service_account.this.email
      timeout         = "15s"
    }
  }
}

resource "google_cloud_scheduler_job" "this" {
  for_each = local.schedule_records

  name        = "${var.env}-mastodon-intermediator-${each.value.job_name}-cron-${each.key}"
  description = each.value.description
  schedule    = each.value.when
  time_zone   = "Europe/London"

  region = var.cloud_region

  http_target {
    http_method = "POST"
    uri = join("", [
      "https://",
      google_cloud_run_v2_job.this[each.value.job_name].location,
      "-run.googleapis.com/apis/run.googleapis.com/v1/namespaces/",
      google_cloud_run_v2_job.this[each.value.job_name].project,
      "/jobs/",
      google_cloud_run_v2_job.this[each.value.job_name].name,
      ":run"
    ])
    oauth_token {
      service_account_email = google_service_account.this.email
      scope                 = "https://www.googleapis.com/auth/cloud-platform"
    }
  }
}