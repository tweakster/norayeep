provider "google" {
  project = var.cloud_project
  region  = var.cloud_region
}